﻿using System;
using System.Collections.Generic;

namespace TestJobsity
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> textList = new List<string>
           {
               "code", "aaagmnrs", "anagrams", "doce", "first", "dude", "irsft", "ddue"
           };

            List<string> listUniqueWords = FunWithAnagrams(textList);
            Console.WriteLine(string.Join(", ", listUniqueWords));
            Console.ReadLine();
        }

        static List<string> FunWithAnagrams(List<string> text)
        {
            List<string> copyList = new List<string>(text);



            foreach (var firstStack in text)
            {
                foreach (var secondStack in text)
                {
                    if (firstStack != secondStack)
                    {
                        IDictionary<char, int> structFirst = GetWordStruct(firstStack);
                        IDictionary<char, int> structSecond = GetWordStruct(secondStack);
                        if (IsSameStruct(structFirst, structSecond))
                        {
                            copyList.Remove(secondStack);
                            return FunWithAnagrams(copyList);
                        }
                    }

                }


            }

            return copyList;
        }

        static bool IsSameStruct(IDictionary<char, int> structWord1, IDictionary<char, int> structWord2)
        {
            foreach(var k in structWord1.Keys)
            {
                if (!structWord2.ContainsKey(k))
                    return false;

                if (structWord1[k] != structWord2[k])
                    return false;
            }
            return true;
        }

        static IDictionary<char, int> GetWordStruct(string word)
        {
 
            char[] wordArray = word.ToCharArray();
            IDictionary<char, int> wordDictionary = new Dictionary<char, int>();

            foreach (var c in wordArray)
            {
                if (wordDictionary.ContainsKey(c))
                    wordDictionary[c] += 1;
                else
                    wordDictionary.Add(c, 1);
            }

            return wordDictionary;
        }
      
    }
}
